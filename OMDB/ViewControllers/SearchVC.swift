//
//  SearchVC.swift
//  OMDB
//
//  Created by Hakkı Yiğit Yener on 11.07.2020.
//  Copyright © 2020 hyyener. All rights reserved.
//

import UIKit
import Alamofire
import MBProgressHUD

class SearchVC: UIViewController {
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchInputTF: UITextField!
    var totalResulCount = 0
    var currentPage = 1
    var isFetching = false
    var dataSource : [SearchResult] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNibs()
    }
    func registerNibs() {
        
        self.searchTable.register(UINib.init(nibName: String(describing: SearchResultCell.self), bundle: nil),
                                  forCellReuseIdentifier: String(describing: SearchResultCell.self))
    }
    
    @IBAction func searchButtonTouched(_ sender: Any) {
        searchInputTF.resignFirstResponder()
        self.currentPage = 1
        self.totalResulCount = 0
        self.dataSource = []
        fetchSearchResult()
    }
    
    func showAlert()  {
        let alertVC = UIAlertController.init(title: "Hata",
                                             message: "Aradığınız kriterlere uygun bir film bulunamadı!",
                                             preferredStyle: .alert)
        let doneAction = UIAlertAction.init(title: "Tamam",
                                            style: .default,
                                            handler: nil)
        alertVC.addAction(doneAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func fetchSearchResult()  {
        guard !isFetching else {
            return
        }
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.isFetching = true
        NetworkManager.shared.fetchSearchResults(searchTerm: searchInputTF.text ?? "",
                                                 page: self.currentPage) { (response) in
                                                    MBProgressHUD.hide(for: self.view, animated: true)
                                                    self.isFetching = false
                                                    guard let response = response else {
                                                        self.dataSource = []
                                                        self.totalResulCount = 0
                                                        self.currentPage = 1
                                                        self.searchTable.reloadData()
                                                        self.showAlert()
                                                        return
                                                    }
                                                    self.dataSource += response.movieList
                                                    self.totalResulCount =  Int.init(response.totalResults) ?? 0
                                                    self.currentPage += 1
                                                    self.searchTable.reloadData()
                                                    if self.currentPage == 2 {
                                                        self.searchTable.scrollToRow(at: IndexPath.init(item: 0, section: 0), at: .top, animated: true)
                                                    }
        }
    }
}

extension SearchVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        NetworkManager.shared.getMovie(imdbId: dataSource[indexPath.row].imdbID) { (movieDetail) in
            MBProgressHUD.hide(for: self.view, animated: true)
            guard let movie = movieDetail else { return }
            let detailVC = UIStoryboard.init(name: StoryBoards.main.rawValue,
                                             bundle: nil).instantiateViewController(withIdentifier: ViewControllers.detail.rawValue) as! MovieDetailVC
            detailVC.movie = movie
            
            self.navigationController?.pushViewController(detailVC, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == dataSource.count-1, totalResulCount > dataSource.count  {
            fetchSearchResult()
        }
    }
}

extension SearchVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SearchResultCell.self), for: indexPath) as! SearchResultCell
        cell.refresCell(with: dataSource[indexPath.row])
        return cell
    }
    
}

extension SearchVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchButtonTouched(textField)
        return true
    }
}
