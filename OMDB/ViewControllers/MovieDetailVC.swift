//
//  MovieDetailVC.swift
//  OMDB
//
//  Created by Hakkı Yiğit Yener on 11.07.2020.
//  Copyright © 2020 hyyener. All rights reserved.
//

import UIKit
import Firebase
import SDWebImage

class MovieDetailVC: UIViewController {
    var movie : MovieDetailModel?
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var plotLabel: UILabel!
    @IBOutlet weak var posterImageHeightConstaint: NSLayoutConstraint!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        logAnalytics()
    }
    
    func setupUI() {
        guard let movie = movie else {
            return
        }
        if let posterURL = URL(string: movie.poster)  {
            posterImageView.sd_setImage(with: posterURL) { (image, error, cacheType, imageURL) in
                if let imageWidth = image?.size.width, let imageHeight = image?.size.height {
                    self.posterImageHeightConstaint.constant =  (UIScreen.main.bounds.width / imageWidth) * imageHeight
                }
            }
        }
        titleLabel.text = movie.title
        plotLabel.text = movie.plot
        yearLabel.text = movie.year
        ratingLabel.text = movie.imdbRating
    }
    
    func logAnalytics() {
        guard let movie = movie else {
            return
        }
        Analytics.logEvent(AnalyticsEventViewItem, parameters: [
            AnalyticsParameterItemID: movie.imdbID,
            AnalyticsParameterItemName: movie.title,
            AnalyticsParameterContentType: movie.type,
            AnalyticsParameterScore: movie.imdbRating,
            ])
    }
}
