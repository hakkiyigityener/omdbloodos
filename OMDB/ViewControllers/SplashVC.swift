//
//  ViewController.swift
//  OMDB
//
//  Created by Hakkı Yiğit Yener on 11.07.2020.
//  Copyright © 2020 hyyener. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

class SplashVC: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    var remoteConfig: RemoteConfig!
    @IBOutlet weak var retryButton: UIButton!
    let loadingPhraseConfigKey = "SplashScreenTitle"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        remoteConfig.configSettings = settings
        if NetworkReachabilityManager()!.isReachable {
            fetchConfig()
        }else {
            retryButton.isHidden = false
        }
        
        
    }
    
    func fetchConfig() {
        remoteConfig.fetchAndActivate { (status, error) in
            DispatchQueue.main.async {
                            if status != .error{
                    print("Config fetched!")
                    self.displayWelcome()
                } else {
                    print("Config not fetched")
                    print("Error: \(error?.localizedDescription ?? "No error available.")")
                    self.retryButton.isHidden = false
                }
            }
        }
    }
    
    func displayWelcome() {
        let welcomeMessage = remoteConfig[loadingPhraseConfigKey].stringValue
        titleLabel.text = welcomeMessage
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            let searchVC = UIStoryboard.init(name: StoryBoards.main.rawValue,
                                             bundle: nil).instantiateViewController(withIdentifier: ViewControllers.search.rawValue) as! SearchVC
            self.navigationController?.setViewControllers([searchVC], animated: true)
        }
    }
    
    @IBAction func retryButtonTouched(_ sender: AnyObject) {
        if NetworkReachabilityManager()!.isReachable {
            retryButton.isHidden = true
            fetchConfig()
        }
    }
}

