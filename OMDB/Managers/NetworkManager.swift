//
//  NetworkManager.swift
//  OMDB
//
//  Created by Hakkı Yiğit Yener on 12.07.2020.
//  Copyright © 2020 hyyener. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    static var shared: NetworkManager = {
        var shared = NetworkManager.init()
        return shared
    }()
    
    func fetchSearchResults(searchTerm:String, page:Int, completionHandler:@escaping (SearchResponse?)->())  {
        let parameters: [String: String] = ["s": searchTerm,
                                            "page": String(describing: page),
                                            "apikey":OMDbApiKey]
        AF.request("https://www.omdbapi.com", parameters: parameters)
            .validate()
            .responseDecodable(of: SearchResponse.self) { (response) in
                guard let films = response.value else {
                    completionHandler(nil)
                    return
                }
                completionHandler(films)
        }
    }
    func getMovie(imdbId:String, completionHandler:@escaping (MovieDetailModel?)->()) {
        let parameters: [String: String] = ["i": imdbId,
                                            "apikey":OMDbApiKey]
        AF.request("https://www.omdbapi.com", parameters: parameters)
            .validate()
            .responseDecodable(of: MovieDetailModel.self) { (response) in
                
                guard let movie = response.value else {
                    completionHandler(nil)
                    return
                }
                completionHandler(movie)
        }
    }
}
