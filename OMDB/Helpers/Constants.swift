//
//  Constants.swift
//  OMDB
//
//  Created by Hakkı Yiğit Yener on 11.07.2020.
//  Copyright © 2020 hyyener. All rights reserved.
//

import Foundation


let OMDbApiKey = "46159457"

enum StoryBoards :String {
    case main = "Main"
}

enum ViewControllers: String {
    case splash = "SplashVC"
    case search = "SearchVC"
    case detail = "MovieDetailVC"
}
