//
//  SearchResultCell.swift
//  OMDB
//
//  Created by Hakkı Yiğit Yener on 11.07.2020.
//  Copyright © 2020 hyyener. All rights reserved.
//

import UIKit
import SDWebImage

class SearchResultCell: UITableViewCell {

    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        posterImageView.sd_cancelCurrentImageLoad()
        posterImageView.image = nil
    }
    func refresCell(with model:SearchResult) {
        titleLabel.text = model.title
        yearLabel.text = model.year
        posterImageView.sd_setImage(with: URL.init(string: model.poster))
    }
    
}
