class MovieRating: Codable {
    let source, value: String

    enum CodingKeys: String, CodingKey {
        case source = "Source"
        case value = "Value"
    }

    init(source: String, value: String) {
        self.source = source
        self.value = value
    }
}
