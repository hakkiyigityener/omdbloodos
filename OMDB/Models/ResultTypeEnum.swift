enum ResultTypeEnum: String, Codable {
    case game = "game"
    case movie = "movie"
    case series = "series"
    case episode = "episode"
}
