class SearchResponse: Codable {
    let movieList: [SearchResult]
    let totalResults, response: String

    enum CodingKeys: String, CodingKey {
        case movieList = "Search"
        case totalResults
        case response = "Response"
    }

    init(movieList: [SearchResult], totalResults: String, response: String) {
        self.movieList = movieList
        self.totalResults = totalResults
        self.response = response
    }
}
